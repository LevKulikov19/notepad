# Блокнот

Необходимо пройти урок: http://developer.alexanderklimov.ru/android/texteditor.php,
после чего доработать приложение.

* Добавить меню определенного содержания и обработать выбор действий

* Изменить настройки Preferences

* Добавить свой шрифт в приложение и дать возможность пользователю выбрать это шрифт (https://developer.android.com/guide/topics/ui/look-and-feel/fonts-in-xml)

Приложение “Блокнот” должен иметь следующий интерфейс:
![Дизайн приложения](doc/notepad_design.jpg)