package com.example.notepad;

import androidx.annotation.ColorInt;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.BaseKeyListener;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    private final static String FILENAME = "sample.txt"; // имя файла
    private EditText mEditText;
    private boolean isEdit = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mEditText = findViewById(R.id.editTextMain);

        // открытие файла
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        boolean isOpen = false;
        try {
            isOpen = prefs.getBoolean(getString(R.string.pref_openmode),false);

        }
        catch (Exception e)
        {
            Toast.makeText(MainActivity.this, "Не удалось использовать параметр настроек открытие файла по умолчанию", Toast.LENGTH_SHORT).show();
        }

        if (isOpen) {
            openFile(FILENAME);
        }

        mEditText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isEdit = true;
            }
        } );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_open:
                openFile(FILENAME);
                return true;
            case R.id.action_save:
                saveFile(FILENAME);
                return true;
            case R.id.action_settings:
                Intent intent = new Intent();
                intent.setClass(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_clear:
                clearWorkspace();
                return true;

            case R.id.action_exit:
                exit();
                return true;
            default:
                return true;
        }
    }

    private void exit () {
        if (isEdit)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Сохранить изменения")
                    .setMessage("Вы действительно закрыть приложение не сохранив изменения?")
                    .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            _exit();
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton("Сохранить", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            _exit();
                            saveFile(FILENAME);
                            dialog.cancel();
                        }
                    })
                    .setCancelable(true);
            builder.show();
        }
        else
            _exit();
    }

    private void _exit() {
        this.finishAffinity();
    }


    // Метод для открытия файла
    private void openFile(String fileName) {
        try {
            InputStream inputStream = openFileInput(fileName);

            if (inputStream != null) {
                InputStreamReader isr = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(isr);
                String line;
                StringBuilder builder = new StringBuilder();

                while ((line = reader.readLine()) != null) {
                    builder.append(line + "\n");
                }

                inputStream.close();
                mEditText.setText(builder.toString());
            }
        } catch (Throwable t) {
            Toast.makeText(getApplicationContext(),
                    "Exception: " + t.toString(),
                    Toast.LENGTH_LONG).show();
        }
    }

    // Метод для сохранения файла
    private void saveFile(String fileName) {
        try {
            OutputStream outputStream = openFileOutput(fileName, 0);
            OutputStreamWriter osw = new OutputStreamWriter(outputStream);
            osw.write(mEditText.getText().toString());
            osw.close();
            isEdit = false;
        } catch (Throwable t) {
            Toast.makeText(getApplicationContext(),
                    "Exception: " + t.toString(),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void clearWorkspace() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Отчистить")
                .setMessage("Вы действительно хотите отчистиь рабочую область?")
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mEditText.setText("");
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.show();
    }

    private void clearDialog () {

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);



        // размер шрифта
        float fSize = 20;
        try {
            fSize = Float.parseFloat(prefs.getString(
                    getString(R.string.pref_size), "20"));
            if (fSize < 10 || fSize > 32) throw new Exception();
        }
        catch (Exception e)
        {
            Toast.makeText(MainActivity.this, "Размер шрифта введен не корректно", Toast.LENGTH_SHORT).show();
        }
        mEditText.setTextSize(fSize);


        // цвет текста
        String red = getResources().getString(R.string.pref_color_red);
        String green = getResources().getString(R.string.pref_color_green);
        String blue = getResources().getString(R.string.pref_color_blue);
        String color = "";
        try {
            color = prefs.getString(getString(R.string.pref_color), "");
        }
        catch (Exception e)
        {
            color = red;
            Toast.makeText(MainActivity.this, "Не удалось использовать выбранный цвет", Toast.LENGTH_SHORT).show();
        }

        int colorFont = R.color.black;
        if (color.contains(red)) colorFont = (R.color.red_800);
        if (color.contains(green)) colorFont = (R.color.green_800);
        if (color.contains(blue)) colorFont = (R.color.blue_800);
        mEditText.setTextColor(getResources().getColor(colorFont));

        // шрифт
        String roboto = getResources().getString(R.string.pref_font_roboto);
        String valisca = getResources().getString(R.string.pref_font_valisca);
        String theater_atlas_deco = getResources().getString(R.string.pref_font_theater_atlas_deco);
        String soledago = getResources().getString(R.string.pref_font_soledago);
        String font = roboto;
        try {
            font = prefs.getString(getString(R.string.pref_font), "");
        }
        catch (Exception e)
        {
            Toast.makeText(MainActivity.this, "Не удалось использовать выбранный шрифт", Toast.LENGTH_SHORT).show();
        }

        Typeface typeface = ResourcesCompat.getFont(this, R.font.roboto);
        if (font.contains(roboto))
            typeface = ResourcesCompat.getFont(this, R.font.roboto);
        if (font.contains(valisca))
            typeface = ResourcesCompat.getFont(this, R.font.valisca);
        if (font.contains(theater_atlas_deco))
            typeface = ResourcesCompat.getFont(this,R.font.theater_atlas_deco);
        if (font.contains(soledago))
            typeface = ResourcesCompat.getFont(this, R.font.soledago);
        mEditText.setTypeface(typeface);

        // фон
        LinearLayout mLayout = (LinearLayout)findViewById(R.id.layoutMain);
        boolean background_d = false;
        try {
            background_d = prefs.getBoolean(getString(R.string.pref_background), false);
        }
        catch (Exception e)
        {
            Toast.makeText(MainActivity.this, "Не удалось использовать выбранный фон", Toast.LENGTH_SHORT).show();
        }
        if (background_d)
            mLayout.setBackgroundResource(R.drawable.background);
        else
            mLayout.setBackgroundColor(getResources().getColor(R.color.white));
    }
}